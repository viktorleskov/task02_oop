package com.leskov.passive.view;

import com.leskov.passive.controller.Controller;
import com.leskov.passive.controller.Controllerdo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new Controllerdo();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print all goods");
        menu.put("2", "  2 - regenerate GoodsList");
        menu.put("3", "  3 - regenerate GoodsList & print all list");
        menu.put("4", "  4 - find goods for you");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        System.out.println(controller.getGoodsList());
    }

    private void pressButton2() {
        System.out.println("Please input list amount:");
        int amount = input.nextInt();
        controller.regenerateGoodsList(amount);
    }

    private void pressButton3() {
        System.out.print("Please input list amount:");
        int amount = Integer.parseInt(input.nextLine());
        controller.regenerateGoodsList(amount);
        System.out.println(controller.getGoodsList());
    }

    private void pressButton4() {
        System.out.println("Please input which goods you want to buy");
        System.out.println("1 - for Tree, 2 - for Apartments, 3 - for House, 4 - for Street");
        ArrayList<Integer> wishlist = new ArrayList<>(Integer.parseInt(input.nextLine()));
        System.out.println(controller.findGoods(wishlist));
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
