package com.leskov.passive.view;

@FunctionalInterface
public interface Printable {
    void print();
}
