package com.leskov.passive.model;

import com.leskov.allitem.Decoration;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Domain {
    private List<Decoration> goodsList;
    private int amount;

    public Domain() {
        goodsList = new ArrayList<>();
        Random rand = new Random();
        amount = (int) (30 * rand.nextDouble());
        generateGoodsList();
    }

    private void generateGoodsList() {
        Random rand = new Random();
        for (int i = 0; i < amount; i++) {
          //  goodsList.add((int) (100 * rand.nextDouble() - 50));
        }
    }

    public void regenerateGoodsList(int amount) {
        this.amount = amount;
        goodsList = new LinkedList<>();
        generateGoodsList();
    }

    public List<Decoration> getGoodsList() {
        return goodsList;
    }

    public List<Decoration> findGoods(ArrayList<Integer> wishlist){
        return goodsList;
    }

    public void setGoodsList(List<Decoration> goodsList) {
        this.goodsList = goodsList;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
