package com.leskov.passive.model;

import com.leskov.allitem.Decoration;

import java.util.ArrayList;
import java.util.List;

public interface Model {
    List<Decoration> getGoodsList();

    void regenerateGoodsList(int amount);
    public List<Decoration> findGoods(ArrayList<Integer> wishlist);
}
