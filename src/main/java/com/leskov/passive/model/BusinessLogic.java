package com.leskov.passive.model;

import com.leskov.allitem.Decoration;

import java.util.ArrayList;
import java.util.List;

public class BusinessLogic implements Model {

    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public List<Decoration> getGoodsList() {
        return domain.getGoodsList();
    }

    @Override
    public void regenerateGoodsList(int amount) {
        if (amount>0){
            domain.regenerateGoodsList(amount);
        }
    }

    @Override
    public List<Decoration> findGoods(ArrayList<Integer> wishlist) {
        return domain.findGoods(wishlist);
    }
}
