package com.leskov.passive.controller;

import com.leskov.allitem.Decoration;

import java.util.ArrayList;
import java.util.List;

public interface Controller {
    List<Decoration> getGoodsList();
    void regenerateGoodsList(int amount);
    List<Decoration> getNewGoodsList(int amount);
    List<Decoration> findGoods(ArrayList<Integer> wishlist);
}
