package com.leskov.passive.controller;

import com.leskov.allitem.Decoration;
import com.leskov.passive.model.BusinessLogic;
import com.leskov.passive.model.Model;

import java.util.ArrayList;
import java.util.List;

public class Controllerdo implements Controller {
    private Model model;

    public Controllerdo() {
        model = new BusinessLogic();
    }

    @Override
    public List<Decoration> getGoodsList() {
        return model.getGoodsList();
    }

    @Override
    public void regenerateGoodsList(int amount) {
        model.regenerateGoodsList(amount);
    }

    @Override
    public List<Decoration> getNewGoodsList(int amount) {
        model.regenerateGoodsList(amount);
        return model.getGoodsList();
    }

    @Override
    public List<Decoration> findGoods(ArrayList<Integer> wishlist) {
        return model.findGoods(wishlist);
    }
}
