package com.leskov;

import com.leskov.passive.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
