package com.leskov.allitem.specialitem;

import com.leskov.allitem.Garland;
import com.leskov.util.PriceList;

public final class TreeGarland extends Garland {
    @Override
    public int price() {
        return PriceList.GARLAND_FOR_TREE;
    }

    @Override
    public boolean isForTree() {
        return true;
    }
}
