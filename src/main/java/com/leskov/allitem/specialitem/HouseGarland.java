package com.leskov.allitem.specialitem;

import com.leskov.allitem.Garland;
import com.leskov.util.PriceList;

public final class HouseGarland extends Garland {
    @Override
    public int price() {
        return PriceList.GARLAND_FOR_HOUSE;
    }

    @Override
    public boolean isForHouse() {
        return true;
    }
}
