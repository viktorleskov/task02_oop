package com.leskov.allitem.specialitem;

import com.leskov.allitem.Figurine;
import com.leskov.util.PriceList;

public final class TreeFigurine extends Figurine {
    @Override
    public int price() {
        return PriceList.FIGURINE_FOR_TREE;
    }

    @Override
    public boolean isForTree() {
        return true;
    }
}
