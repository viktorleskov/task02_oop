package com.leskov.allitem.specialitem;

import com.leskov.allitem.Garland;
import com.leskov.util.PriceList;

public final class ApartmentGarland extends Garland {
    @Override
    public int price() {
        return PriceList.GARLAND_FOR_APARTMENT;
    }

    @Override
    public boolean isForApartment() {
        return true;
    }
}
