package com.leskov.allitem.specialitem;

import com.leskov.allitem.Wreath;
import com.leskov.util.PriceList;

public final class HouseWreath extends Wreath {
    @Override
    public int price() {
        return PriceList.WREATH_FOR_HOUSE;
    }

    @Override
    public boolean isForHouse() {
        return true;
    }
}
