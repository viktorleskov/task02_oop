package com.leskov.allitem.specialitem;

import com.leskov.allitem.Twig;
import com.leskov.util.PriceList;

public final class TreeTwig extends Twig {
    @Override
    public int price() {
        return PriceList.TWIG_FOR_TREE;
    }

    @Override
    public boolean isForTree() {
        return true;
    }
}
