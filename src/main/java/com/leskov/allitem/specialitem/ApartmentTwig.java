package com.leskov.allitem.specialitem;

import com.leskov.allitem.Twig;
import com.leskov.util.PriceList;

public final class ApartmentTwig extends Twig {
    @Override
    public int price() {
        return PriceList.TWIG_FOR_APARTMENT;
    }

    @Override
    public boolean isForApartment() {
        return true;
    }
}
