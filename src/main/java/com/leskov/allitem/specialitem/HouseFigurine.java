package com.leskov.allitem.specialitem;

import com.leskov.allitem.Figurine;
import com.leskov.util.PriceList;

public final class HouseFigurine extends Figurine {
    @Override
    public int price() {
        return PriceList.FIGURINE_FOR_HOUSE;
    }

    @Override
    public boolean isForHouse() {
        return true;
    }
}
