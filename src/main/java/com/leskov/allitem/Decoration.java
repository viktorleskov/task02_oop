package com.leskov.allitem;

public interface Decoration {
    public int price();
    default boolean isForTree(){ return false; }
    default boolean isForHouse(){ return false; }
    default boolean isForApartment(){ return false; }
    default boolean isForStreet(){ return false; }
}
