package com.leskov.util;

public final class PriceList {
    public static final int GARLAND_FOR_HOUSE = 200;
    public static final int GARLAND_FOR_APARTMENT = 150;
    public static final int GARLAND_FOR_TREE = 100;

    public static final int TWIG_FOR_APARTMENT = 100;
    public static final int TWIG_FOR_TREE = 50;

    public static final int FIGURINE_FOR_HOUSE = 150;
    public static final int FIGURINE_FOR_TREE = 50;

    public static final int WREATH_FOR_HOUSE = 200;
}
